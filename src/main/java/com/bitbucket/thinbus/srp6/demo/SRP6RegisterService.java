package com.bitbucket.thinbus.srp6.demo;

import java.util.concurrent.ConcurrentHashMap;

import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

/**
 * A javax.ws.rs RESTful Service which returns JSON and which implements the
 * backed to the page ported from https://github.com/RuslanZavacky/srp-6a-demo
 */
@Path("/")
public class SRP6RegisterService {

	/**
	 * Clearly you need to use a database but for this demo we will just hold
	 * things in memory. Here we hold the registered user details in a map using
	 * the email as the key.
	 * 
	 * Note: In a real database make the salt a 'not null' field with a unique
	 * constraint as per thinbus README.md
	 */
	final static ConcurrentHashMap<String, UserDetail> fakeUserRepository = new ConcurrentHashMap<>();

	@POST
	@Path("/srp-6a-demo/register")
	@Produces("application/json")
	public Response getUserById(@FormParam("email") String email, @FormParam("password_salt") String salt,
			@FormParam("password_verifier") String verifier) {
		UserDetail ud = new UserDetail(email, salt, verifier);
		fakeUserRepository.put(ud.getEmail(), ud);
		return Response.status(200).entity(ud).build();
	}

}