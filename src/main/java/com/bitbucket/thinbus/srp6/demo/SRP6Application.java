package com.bitbucket.thinbus.srp6.demo;

import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

/**
 * javax.ws.rs entry point for the system configured in web.xml
 */
public class SRP6Application extends Application {

	/**
	 * Returns all the javax.ws.rs end points
	 */
	@Override
	public Set<Class<?>> getClasses() {
		HashSet<Class<?>> set = new HashSet<>();
		set.add(SRP6RegisterService.class);
		set.add(SRP6LoginService.class);
		return set;
	}

}
