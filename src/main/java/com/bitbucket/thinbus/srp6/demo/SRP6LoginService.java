package com.bitbucket.thinbus.srp6.demo;

import static com.nimbusds.srp6.BigIntegerUtils.toHex;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.ServletContext;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.bitbucket.thinbus.srp6.js.HexHashedClientEvidenceRoutine;
import com.bitbucket.thinbus.srp6.js.HexHashedRoutines;
import com.bitbucket.thinbus.srp6.js.HexHashedServerEvidenceRoutine;
import com.bitbucket.thinbus.srp6.js.SRP6JavascriptServerSession;
import com.bitbucket.thinbus.srp6.js.SRP6JavascriptServerSessionSHA256;
import com.nimbusds.srp6.BigIntegerUtils;
import com.nimbusds.srp6.SRP6ClientCredentials;
import com.nimbusds.srp6.SRP6ClientSession;
import com.nimbusds.srp6.SRP6CryptoParams;
import com.nimbusds.srp6.SRP6Exception;
import com.nimbusds.srp6.URoutine;
import com.nimbusds.srp6.URoutineContext;
import com.nimbusds.srp6.XRoutine;
import com.nimbusds.srp6.XRoutineWithUserIdentity;

/**
 * A javax.ws.rs RESTful Service which returns JSON and which implements the
 * backed to the page ported from https://github.com/RuslanZavacky/srp-6a-demo
 */
@Path("/")
public class SRP6LoginService {

	/**
	 * Used to lookup N and g set in web.xml
	 */
	@Context
	ServletContext context;

	/**
	 * This is a javax.ws.rs service shared by any active users. In order to do
	 * the challenge response we need to store the SRP6JavascriptServerSession
	 * for each user. You could use the HTTPSession. Here we will just hold
	 * things in memory using the email as the key
	 */
	final static ConcurrentHashMap<String, SRP6JavascriptServerSession> fakeLoginSessions = new ConcurrentHashMap<>();

	@GET
	@Path("/srp-6a-demo/test")
	@Produces("application/json")
	public Response test() throws Exception {

		String N = context.getInitParameter("N");
		String g = context.getInitParameter("g");
		
		String username = "ruslan.zavackiy@gmail.com";
		String password = "random-password";
		
		SRP6ClientSession client = new SRP6ClientSession();
		
		client.setClientEvidenceRoutine(new HexHashedClientEvidenceRoutine());
		client.setHashedKeysRoutine(new HexHashedURoutine());
		client.setServerEvidenceRoutine(new HexHashedServerEvidenceRoutine());
		client.setXRoutine(new MyXRoutineWithUserIdentity());
		
		client.step1(username, password);
		
		Response step1response = login(username, null, null);
		SRP6Response srpStep1 = (SRP6Response)step1response.getEntity();
		System.out.println("B    : " + srpStep1.getB());
		System.out.println("Salt : " + srpStep1.getSalt());

		// 
		
//		XRoutine xroutine = new MyXRoutineWithUserIdentity();
//		MessageDigest digest = MessageDigest.getInstance("SHA-256");
//		BigInteger x = xroutine.computeX(digest, srpStep1.getSalt().getBytes(), username.getBytes(), password.getBytes());
//		
//		System.out.println("--> X : " + x);
		
		// 
		
		SRP6CryptoParams config = new SRP6CryptoParams(new BigInteger(N, 10), new BigInteger(g, 10), "SHA-256");
		SRP6ClientCredentials cred = null;

		try {
		        cred = client.step2(config,BigIntegerUtils.fromHex(srpStep1.getSalt()), BigIntegerUtils.fromHex(srpStep1.getB()));
		} catch (SRP6Exception e) {
			return Response.status(401).entity(e).build();
		}
				
		System.out.println("A    : " + cred.A);
		System.out.println("M1    : " + cred.M1);
		
		Response step2response = login(username, BigIntegerUtils.toHex(cred.A), BigIntegerUtils.toHex(cred.M1));
		
		System.out.println("step2response  : " + step2response.getStatus() + " " + step2response.getEntity());
		
		return Response.status(200).entity(step2response.getStatus() + " " + step2response.getEntity()).build();
	}
	
	@POST
	@Path("/srp-6a-demo/login")
	@Produces("application/json")
	public Response login(@FormParam("email") String email, @FormParam("A") String A, @FormParam("M1") String M1) throws Exception {

		String N = context.getInitParameter("N");
		String g = context.getInitParameter("g");

		UserDetail ud = SRP6RegisterService.fakeUserRepository.get(email);
		if (ud == null) {
			System.err.println("user is not registered: "+email);
			return Response.status(Status.FORBIDDEN).build();
		}

		if (notValid(A, M1)) {
			System.err.println("A and M1 must both be left undefined or must both be defined");
			return Response.status(Status.FORBIDDEN).build();
		}

		SRP6JavascriptServerSession srpSession = null;
		
		if (notDefined(A)) {
			// browser is attempting initial login possibly after a page refresh
			// we
			// create a fresh server session
			srpSession = new SRP6JavascriptServerSessionSHA256(N, g);
			fakeLoginSessions.put(email, srpSession);
			
			try {
				String B = srpSession.step1(ud.getEmail(), ud.getSalt(), ud.getVerifier());
				System.out.println(String.format("session state created then moved to %s", srpSession.getState()));
				return Response.status(200).entity(new SRP6Response(ud.getSalt(), B)).build();
			} catch (Exception e) {
				System.err.println(e);
				return Response.status(Status.FORBIDDEN).build();
			}
		} else {
			// browser is responding to a challenge and is sending the proof of
			// password
			srpSession = fakeLoginSessions.get(email);

			if(srpSession.getState().equals("STEP_1")) {
				try {
					String M2 = srpSession.step2(A, M1);
					System.out.println(String.format("session state moved to %s", srpSession.getState()));
					return Response.status(200).entity(new SRP6Response(M2)).build();
				} catch (Exception e) {
					System.err.println(e);
					return Response.status(Status.FORBIDDEN).build();
				}
			} else {
				System.err.println("user is trying to pass A and M1 but we are not in state STEP_1 reload the browser page and start login from beginning");
				return Response.status(Status.FORBIDDEN).build();
			}
		}
	}

	/**
	 * 
	 * @param A
	 *            The browser ephemeral key
	 * @return Whether A is not null and not blank.
	 */
	private boolean notDefined(String A) {
		if (A == null || A.equals("")) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Both must be defined to be valid
	 * 
	 * @param A
	 *            The browser ephemeral key
	 * @param M1
	 *            The browser proof of the shared session key.
	 * @return
	 */
	private boolean notValid(String A, String M1) {
		if (A == null) {
			if (M1 == null) {
				return false;
			} else {
				return true;
			}
		} else if (M1 == null) {
			return true;
		}

		if (A == "") {
			if (M1 == "") {
				return false;
			} else {
				return true;
			}
		} else if (M1 == "") {
			return true;
		}

		return false;
	}
	
	
	private class HexHashedURoutine implements URoutine {

		/**
		 * Computes the random scrambling parameter u = H(A | B)
		 * 
		 * @param digest
		 *            The hash function 'H'. Must not be {@code null}.
		 * @param A
		 *            The public client value 'A'. Must not be {@code null}.
		 * @param B
		 *            The public server value 'B'. Must not be {@code null}.
		 * 
		 * @return The resulting 'u' value as as 'H( HEX(A) | HEX(B) )'.
		 */
		@Override
		public BigInteger computeU(SRP6CryptoParams cryptoParams, URoutineContext ctx) {
			return HexHashedRoutines.hashValues(cryptoParams.getMessageDigestInstance(), toHex(ctx.A), toHex(ctx.B));
		}
	}
	
	
	private class MyXRoutineWithUserIdentity implements XRoutine {


		/**
		 * Computes x = H(s | H(I | ":" | P))
		 *
		 * <p>This method complies with the RFC 5054 recommendation, save for 
		 * the hash algorithm which can be any (RFC 5054 recommends SHA-1).
		 *
		 * @param digest   The hash function 'H'. Must not be {@code null}.
		 * @param salt     The salt 's'. Must not be {@code null}.
		 * @param username The user identity 'I'. Must not be {@code null}.
		 * @param password The user password 'P'. Must not be {@code null}.
		 *
		 * @return The resulting 'x' value.
		 */
		public BigInteger computeX(final MessageDigest digest,
					   final byte[] salt,
					   final byte[] username,
					   final byte[] password) {
			
			BigInteger s = new BigInteger(salt);
		
			System.out.println("salt " + bytesToHex(salt));
			System.out.println("s" + s);
			System.out.println("username " +  new String(username));
			System.out.println("password " +  new String(password));
			
			digest.update(username);
			digest.update((byte)':');
			digest.update(password);
			
			byte[] hash1 = digest.digest();
			
			System.out.println("hash1 " +  bytesToHex(hash1));
			
			String hashStr = s.toString(16) + bytesToHex(hash1);
			
			
			
			System.out.println("hashStr " +  hashStr);
			
			digest.reset();
			digest.update(hashStr.toUpperCase().getBytes());
			byte[] hash = digest.digest();

			
			System.out.println("hash " +  bytesToHex(hash));

			System.out.println("final " + new BigInteger(1, hash));
			
			return new BigInteger(1, hash);
		}
		
		final protected char[] hexArray = "0123456789abcdef".toCharArray();
		
		public String bytesToHex(byte[] bytes) {
		    char[] hexChars = new char[bytes.length * 2];
		    for ( int j = 0; j < bytes.length; j++ ) {
		        int v = bytes[j] & 0xFF;
		        hexChars[j * 2] = hexArray[v >>> 4];
		        hexChars[j * 2 + 1] = hexArray[v & 0x0F];
		    }
		    return new String(hexChars);
		}
		
		/**
		 * Returns a string representation of this routine algorithm.
		 * 
		 * @return "H(s | H(I | ":" | P))"
		 */
		public String toString() {
		
			return "H(s | H(I | \":\" | P))";
		}
	}
}