load("src/test/resources/TestUtils.js");

var start = Date.now();
var test_random16byteHexAdvance = 0;

load("src/main/webapp/resources/lib/biginteger.js");
load("src/main/webapp/resources/lib/sha256.js");
load("src/main/webapp/resources/lib/srp.js");
load("src/main/webapp/resources/lib/isaac.js");
load("src/main/webapp/resources/lib/random.js");

var SrpServerSession = Packages.com.github.simbo1905.srp.SrpServerSession;
var javaSrpClientSession = Packages.com.github.simbo1905.srp.NotRandomSrpClientSession;
var SrpServerSession = Packages.com.github.simbo1905.srp.SrpServerSession;
var javaBigInteger = Packages.java.math.BigInteger;
var javaHex = Packages.com.github.simbo1905.srp.Hex;

var sN_1024 = "EEAF0AB9ADB38DD69C33F80AFA8FC5E86072618775FF3C0B9EA2314C9C256576D674DF7496EA81D3383B4813D692C6E0E0D5D8E250B98BE48E495C1D6089DAD15DC7D7B46154D6B6CE8EF4AD69B15D4982559B297BCF1885C529F566660E57EC68EDBC3C05726CC02FD4CBF4976EAA9AFD5138FE8376435B9FC61D2FC0EB06E3";
var sg_1024 = "2";

function toHex(n) {
	return n.toString(16).toUpperCase();
}

var a = "238c58da24ac4acea119e0c418ee0fc0";
var s = "132ce4591a29220827c6198169ea4320";
var I = "tom@arcot.com";
var P = "password1234";

tests({

	// just a sanity check
	isaacGivesNumberLessThanOrEqualToOne: function() {
		assert.assertTrue( isaac.random() <= 1.0 );
	},

	// just a sanity check
	random16byteHex: function() {
        var random = random16byteHex.random();
        assert.assertTrue( random  !== "" );
	},
	
	testJavaHashMatchesJavascriptHash: function() {
		var random = random16byteHex.random();
		var jsInteger = new BigInteger(random, 16);
		var jInteger = new javaBigInteger(random, 16);
		assert.assertTrue(jInteger.toString(16).toUpperCase() == jsInteger.toString(16).toUpperCase());
		var jsHash = SHA256(jsInteger.toString(16));
		var jHash = javaSrpClientSession.hash(jInteger.toString(16));
		assert.assertEquals(""+jHash, ""+jsHash);
		var jsBytes = javaHex.decodeToByteArray(jsHash);
		var jBytes = javaSrpClientSession.hashBytes(jInteger.toString(16));
		assert.assertEquals(jBytes.length, jsBytes.length, 0.000001);
		for( var i = 0; i < jBytes.length; i++ ) {
			var jb = jBytes[i];
			var jsb = jsBytes[i];
			assert.assertEquals(jb, jsb, 0.000001);
		}
	},
	
	testClientCatchesBadB: function() {
		var bClient = SrpClientSession;
		bClient.init(sN_1024,sg_1024);
		try {
			bClient.calculateSecret("0", s, I, P);
			fail();
		} catch(e) {
			// good
		} 	
		try {
			bClient.calculateSecret(sN_1024, s, I, P);
			fail();
		} catch(e) {
			// good
		} 			
	},
	
	testJavascriptClientAgainstJava: function() {
	
		var jClient = new javaSrpClientSession(sN_1024,sg_1024);
		var jA = jClient.generateClientCredentials(a); // can only pass 'a' to test class
		
		var bClient = SrpClientSession;
		bClient.init(sN_1024,sg_1024);
		
		var bA = bClient.generateClientCredentials(a);	// do not pass 'a' outside of tests
		/*
		
		//println("assert jA == bA");
		assert.assertEquals(jA, toHex(bA));
		
		var jv = jClient.generateVerifier(s, I, P);
		var bv = bClient.generateVerifier(s, I, P, a); // do not pass 'a' outside of tests
		//println("assert jv == bv");
		assert.assertEquals(jv, toHex(bv));
		
		var server = new SrpServerSession(sN_1024,sg_1024,jv);
		var B = server.generateServerCredentials();
		
		jClient.calculateSecret(B, s, I, P);
		var jM1 = jClient.generateSecretHashM1();
		
		bClient.calculateSecret(""+B, s, I, P);
		var bM1 = bClient.generateSecretHashM1();
		
		//println("assert jM1 == bM1");
		assert.assertEquals(jM1, bM1);

		//println("calculate sM2");
		server.calculateSecret(toHex(bA));
		var sM2 = server.generateSecretHashM2(bM1);

		//println("verify M2 java");
		jClient.validateSessionHashM2(sM2);
		
		//println("verify M2 javascript");
		bClient.validateSessionHashM2(sM2);
		
		var end =  Date.now();
		//println("duration:"+(end-start));		
		*/
	}
	
});

